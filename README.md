# Welcome to Aurora

Built with Unreal Engine v4.21.1, Aurora is an interactive adventure demo.

## Info

+ Built using Unreal Engine Version 4.21.1
+ Collaborators:
  + [Tilman R.](https://github.com/medallyon)
  + [Paul M.](https://github.com/Linkitu7)
  + [Michalis A.](https://github.com/MichalisAntonas)

## How to play

Head over to the [**Releases**](https://github.com/Medallyon/aurora/releases) section of this repository and download the latest release. A `Release` is composed of the following:

+ A zipped archive containing the game executable and any required libraries
  + Download and un-zip this archive to anywhere on your computer, and run `aurora.exe`
+ The source code for this demo

## Asset Packs

+ Starter Content
+ [Advanced Locomotion V3](https://www.unrealengine.com/marketplace/en-US/item/61b44ccf83234ba99cdada39a7460abc)
+ [Advanced Village Pack](https://www.unrealengine.com/marketplace/en-US/item/def870df86a145b8817bba409d87b051)
+ [Ancient Treasures](https://www.unrealengine.com/marketplace/en-US/item/368aca6c4e7448ed8fecd31288da4879)
+ [Animal Variety Pack](https://www.unrealengine.com/marketplace/en-US/item/c661d0a956454ea4ba6d12c09a687406)
+ [Aurora Borealis](https://www.unrealengine.com/marketplace/en-US/item/9a779651cb834ae6af8394b189c17e5b)
+ [Brushify](https://www.unrealengine.com/marketplace/en-US/item/9af8943b537a4bc0a0cb962bccb0d3cd)
+ [Fantasy Orchestral](https://www.unrealengine.com/marketplace/en-US/item/e7d1b9fa13ce4e8680e36bc3756a43b7)
+ [Infinity Blade Icelands](https://www.unrealengine.com/marketplace/en-US/item/da3c5430dc5846d3a0ec9aa0d631b820)
+ [Infinity Blade Warriors](https://www.unrealengine.com/marketplace/en-US/item/33c13829c0da48199315e9adf8d33c73)
+ [PhotoRealistic Backgrounds](https://www.unrealengine.com/marketplace/en-US/item/8c66175576b5405fbb43315285f3533b)
+ [Open World Foliage](https://www.unrealengine.com/marketplace/en-US/item/d395cdbaece6431abe3f153ccd432857)
+ [Water Materials](https://www.unrealengine.com/marketplace/en-US/item/2ed5823f48fe41b5ae826841fe11b023)
